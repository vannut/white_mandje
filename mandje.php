<?php

try {
	
	require 'vendor/autoload.php';
	
	//- De bovenkant van de html pagina waarop de bonnen getoond worden
			include('mandje_top.php');


	//- mandje 1
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR');
			$mandje->voegToe('CL');
			$mandje->verwerk();

			include('mandje_bon.php');

	//- mandje 2
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR', 6);
			$mandje->voegToe('CL', 3);
			$mandje->verwerk();

			include('mandje_bon.php');

			echo "<div class='clearfix'></div>";
	//- mandje 3
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR', 5);
			$mandje->voegToe('SG', 14);
			$mandje->verwerk();

			include('mandje_bon.php');

	//- mandje 4
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CD', 7);
			$mandje->voegToe('SG', 30);
			$mandje->verwerk();

			include('mandje_bon.php');
			echo "<div class='clearfix'></div>";
	//- mandje 5
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR', 4);
			$mandje->voegToe('SG', 10);
			$mandje->voegToe('BB', 1);
			$mandje->verwerk();

			include('mandje_bon.php');

	//- mandje 6
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR', 10);
			$mandje->voegToe('CL', 15);
			$mandje->voegToe('CM', 5);
			$mandje->voegToe('BB', 2);
			$mandje->voegToe('SG', 45);

			$mandje->verwerk();

			include('mandje_bon.php');
			echo "<div class='clearfix'></div>";

	//- mandje 7
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CD', 20);

			$mandje->verwerk();

			include('mandje_bon.php');
	
	//- mandje 8
			$mandje = new \Vannut\mandje();

			$mandje->voegToe('CR', 4);
			$mandje->voegToe('SA', 1);
			$mandje->voegToe('WA', 1);

			$mandje->verwerk();

			include('mandje_bon.php');



	

	//- de onderkant van de html pagina
			include('mandje_bottom.php');


} catch (Exception $e) {
	
	print $e->getMessage();

}


?>
