<?php
/**
 *
 *		Winkelmandje voor
 *					Doosje Chocolade
 *      		
 *
 *
 *	Aannames:
 *		- bedragen in centen
 * 		- Er is genoeg voorraad om aan alle aanvragen te voldoen.
 *   	- Na minimale afname is iedere natuurlijke integer als aantal geoorloofd.
 *    - Geen opsplitsing van BTW tarieven
 *    - Strooigoed gaat per ons
 *    - Ieder item heeft een prijs
 *    	"koop er drie betaal er twee"-varianten worden met een korting ter grootte van 
 *    	1 eenheid verrekend
 *    - foutafhandeling dmv exceptions
 *    - De aanbiedingen  (sinterklaas/winter aanbiedingen) zijn 'gewoon' artikelen.
 *
 * 
 * 			
 * @author Richard Verbruggen <richard@vannut.nl>
 */

namespace Vannut;

class Mandje {
	
	private $voorraad; 	//- Bevat een overzicht van de voorraad van de winkel.
	public	$mandje;		//- bevat de items van het mandje
	public $korting;		//- container voor de kortingen

	public function __construct() {
			//- De artikelen en hun gegevens worden hier nu statisch neergezet:
					$this->voorraad = array();
					$this->voorraad['CR'] 	= array('prijs' => 250,
																					'eenheid' => array('reep','repen'),
																					'omschrijving'=> 'Chocolade Reep',
																					'min' => 1
																				);
					$this->voorraad['SG'] 	= array('prijs' => 40,
																					'eenheid' => array('ons','ons'),
																					'omschrijving'=> 'Strooigoed',
																					'min' => 1
																				);
					$this->voorraad['BB'] 	= array('prijs' => 500,
																					'eenheid' => array('doosje','doosjes'),
																					'omschrijving'=> 'Bonbons',
																					'min' => 1
																				);
					$this->voorraad['CL'] 	= array('prijs' => 300,
																					'eenheid' => array('letter','letters'),
																					'omschrijving'=> 'Chocolade Letter',
																					'min' => 1
																				);
					$this->voorraad['CD'] 	= array('prijs' => 100,
																					'eenheid' => array('stuk','stuks'),
																					'omschrijving'=> 'Chocolade Donuts',
																					'min' => 1
																				);
					$this->voorraad['CM'] 	= array('prijs' => 100,
																					'eenheid' => array('pak','pakken'),
																					'omschrijving'=> 'Chocolade Melk',
																					'min' => 1
																				);
					$this->voorraad['SA'] 	= array('prijs' => 1300,
																					'eenheid' => array('aanbieding','aanbieding'),
																					'omschrijving'=> 'Sinterklaas Aanbieding: 3 chocolade letters + een kilo strooigoed met gratis doosje bonbons',
																					'min' => 1
																				);
					$this->voorraad['WA'] 	= array('prijs' => 1250,
																					'eenheid' => array('aanbieding','aanbieding'),
																					'omschrijving'=> 'Winter Aanbieding: 6 chocolade letters & 3 pakken chocolade melk',
																					'min' => 1
																				);

			//- mandje opzetten
					$this->mandje = array(	'artikelen'=>array(), 
																	'korting'=>array(),
																	'totalen'=> array('subtotaal'=>0,'korting'=>0)
																);

			//- Korting schema's / regels
					$this->korting = array();
						$this->korting['CR'] = array( 3 	=> array('per'=>3, 	'korting_bedrag' => -250, 'omschrijving' => '3 repen voor de prijs van 2') );
						$this->korting['CL'] = array( 3 	=> array('per'=>3, 	'korting_bedrag' => -200, 'omschrijving' => '3 Letters voor €7') );
						$this->korting['CD'] = array(	12 	=> array('per'=>12, 'korting_bedrag' => -600, 'omschrijving' => 'Dozijn Donut korting'),
																					6 	=> array('per'=>6, 	'korting_bedrag' => -200, 'omschrijving' => 'Half Dozijn Donut korting')
																				);
	}


	/**
	 * Voeg een artikel toe aan het mandje
	 * @param  string  $artikel artikel'nummer'
	 * @param  integer $aantal  hoeveel?
	 * @return boolean           gelukt?
	 */	
			public function voegToe($artikel, $aantal = 1){
					$artikel = strtoupper($artikel);
					
					//- bestaat het artikel wel?
							if (!array_key_exists($artikel, $this->voorraad))
											throw new Exception("[mandje] Artikel (".$artikel.") niet gevonden", 1);

					//- Minimale afname? 
							if($aantal < $this->voorraad[$artikel]['min'])
								$aantal = $this->voorraad[$artikel]['min'];
							

					//- toevoegen
							if(!isset($this->mandje[$artikel]))  //- nog niet in het mandje aanwezig
									$this->mandje['artikelen'][$artikel] = array('aantal'=>$aantal);
							else  //- wel aanwezig in mandje, dus aantal updaten
								$this->mandje['artikelen'][$artikel]['aantal'] = $this->mandje['artikelen'][$artikel]['aantal'] + $aantal;
			}




	/**
	 * Verwerken van het mandje
	 * lees: prijzen bepalen, omschrijvingen toevoegen en eventuele korting regels toepassen
	 * @return null 
	 */
			public function verwerk(){
					//- valt er wel iets te verwerken?
							if(!is_array($this->mandje['artikelen']) || count($this->mandje['artikelen']) <= 0)
									throw new Exception("Geen artikelen in mandje, dus niet te verwerken", 5);
							
					//- Alle artikelen aflopen
							foreach($this->mandje['artikelen'] AS $art => &$gegevens){   //- &reference
								//- De prijs
										$gegevens['eenheid_prijs'] = $this->voorraad[$art]['prijs'];
										$gegevens['prijs'] = $gegevens['aantal'] * $this->voorraad[$art]['prijs'];
								
								//- omschrijving
										$gegevens['omschrijving'] = $this->voorraad[$art]['omschrijving'];
								
								//- eenheid
										$gegevens['eenheid'] = $this->voorraad[$art]['eenheid'][0];
										
								//- totalen
										$this->mandje['totalen']['subtotaal'] = (int) $this->mandje['totalen']['subtotaal'] + (int) $gegevens['prijs'];

								//- Kortingen
										//- bestaat er een kortings regel voor dit product?
												if(array_key_exists($art, $this->korting)){
													
													
													//- sorteer de kortingen op de key van groot naar klein
															krsort($this->korting[$art]);
															//- teller om te zien hoeveel we nog over hebben		
																	$tellen = $gegevens['aantal'];
															
															//- doorlopen van de korting staffels
																	foreach($this->korting[$art] AS $per => $gev){
																		//- zit er genoeg in het mandje om een korting te krijgen?
																				$vloer = floor($tellen/$per);
																				if($vloer >= 1){
																					//- Bedrag berekenen
																							$bedrag = ($vloer * $gev['korting_bedrag']);
																					//- Korting opslaan als regel
																							$this->mandje['korting'][] = array(	'omschrijving'=>$gev['omschrijving'],
																																									'bedrag'=> $bedrag,
																																									'aantal'=> $vloer
																																								);
																					//- en de totalen optellen
																							$this->mandje['totalen']['korting'] = $this->mandje['totalen']['korting'] + $bedrag;

																					//- Hoeveel eenheden blijven er over om een evnetuele kleinere staffel te gebruiken??
																							$tellen = $tellen - ($vloer * $per);
																				}
																	}
													
												} 
							}

					//- Grand totalen 
							$this->mandje['totalen']['grandtotaal'] = $this->mandje['totalen']['subtotaal'] + $this->mandje['totalen']['korting'];
			}


		/**
		 * Helper functie om een 'opgemaakte' prijs te krijgen uit een hoeveelheid centen
		 */
			public function prijs($int){
				return sprintf('%.02F', $int/100);
			}


}

?>